<?PHP
     session_start();

     $mesas = simplexml_load_file("mesas.xml");
	foreach ($mesas as $mesa) {
		if ($mesa['id']== $_SESSION["mesa"]) {
            $mesa->estado = '1';
			$mesas->asXML("mesas.xml");
		}
	}

     session_destroy();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Finalizado</title>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
	<center>
	<div class="container">
		<div class="card mt-5" >
		  <div class="card-body">
		    <h1 class="mt-3">Sesion finalizada</h1>
		    <a href="login.php" class="mt-2 mb-3">Volver al login</a>
		  </div>
		</div>
	</div>
	</center>

	<script src="js/jquery-3.4.1.min.js"></script>
	<script src="js/popper.min.js"></script>
  	<script src="js/bootstrap.min.js"></script>

</body>
</html>


