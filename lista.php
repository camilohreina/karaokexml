<?PHP include ("seguridad.php");?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>Karaoke</title>
</head>
<body>
    <ul class="nav justify-content-center mt-3">
        <li class="nav-item">
          <a class="nav-link active" href="catalogo.php">Catalogo</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="lista.php">Lista</a>
        </li>
        <form action="salir.php" method="POST">
          <button class="btn btn-danger" type="submit">Salir</button>
        </form>
    </ul>
    <div class="container">
        <h1 class="mt-3" >KARAOKE XML - LISTA </h1>
        <!-- <button class="btn btn-primary float-lg-right" >Agregar Cancion</button> -->
        <br>
        <br>
        <hr>
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Cancion</th>
              <th scope="col">Artista</th>
              <th scope="col">Genero</th>
              <th scope="col">Mesa</th>
              <th scope="col">Accion</th>
            </tr>
          </thead>
          <tbody>
            
            <?php

              $listas=simplexml_load_file("ListaDeReproduccion.xml");
              foreach ($listas as $lista) {
                echo '<tr>';
                echo '<th scope="row">'.$lista['id'].'</th>';
                echo '<td>'.$lista->cancion.'</td>';
                echo '<td>'.$lista->artista.'</td>';
                echo '<td>'.$lista->genero.'</td>';
                echo '<td>'.$lista->mesa.'</td>';
                echo '<td><button type="button" onclick="consultar('.$lista['id'].')" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                         Reproducir
                      </button><td>';
                echo '</tr>';
              }

              ?>
          </tbody>
        </table>
    </div>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Reproduccion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body" id="respuesta">
      </div>
    </div>
  </div>
</div>


    <!-- Librerias JS -->
	<script src="js/jquery-3.4.1.min.js"></script>
	<script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script>
    function consultar(id){
    $.ajax({
        type: "POST",
        dataType: 'html',
        url: "consultar.php",
        data: "id="+id,
        success: function(resp){
              document.getElementById('respuesta').innerHTML=`<iframe width="560" height="315" src="${resp}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
            }
        });
    }
  </script>
  
</body>
</html>