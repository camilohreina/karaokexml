<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
	<center>
	<div class="container">
		<?php 
			if ($_GET){	
				if ($_GET["error"] == "si"){?> 
				<div class="alert alert-danger" role="alert">
				Esta mesa esta ocupada, elige otra!
				</div>
		<?php }}?>
		<div class="card mt-5" style="width: 18rem;">
		  <div class="card-body">
		    <form action="entrar.php" method="POST">
			  <div class="form-group" >
				<h1 class="mb-3">LogIn</h1>
			    <label for="exampleInputEmail1">Elija la mesa</label>
			    <input type="number" class="form-control" name="mesa" placeholder="Ingrese la mesa del 1 - 10">
			  </div>
			  <button class="btn btn-info" type="submit">Ingresar</button>
			</form>
		  </div>
		</div>
	</div>
	</center>

	<script src="js/jquery-3.4.1.min.js"></script>
	<script src="js/popper.min.js"></script>
  	<script src="js/bootstrap.min.js"></script>

</body>
</html>
