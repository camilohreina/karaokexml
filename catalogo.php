<?PHP include ("seguridad.php");?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>Karaoke</title>
</head>
<body>
    <ul class="nav justify-content-center mt-3">
        <li class="nav-item">
          <a class="nav-link active" href="catalogo.php">Catalogo</a>
        </li>
        <?php
        if($_SESSION["mesa"]==10){

          ?>
        <li class="nav-item">
          <a class="nav-link" href="lista.php">Lista</a>
        </li>
        <?php
        }
         ?>
        <form action="salir.php" method="POST">
          <button class="btn btn-danger" type="submit">Salir</button>
        </form>

    </ul>
    <div class="container">
        <h1 class="mt-3" >KARAOKE XML - MESA <?PHP echo $_SESSION["mesa"];?></h1>
        <?php
        if($_SESSION["mesa"]==10){

          ?>
        <a type="button" class="btn btn-primary float-lg-right" href="nuevo.php">Agregar Cancion</a >
        <?php
        }
         ?>
        <br>
        <br>
        <hr>
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Cancion</th>
              <th scope="col">Artista</th>
              <th scope="col">Genero</th>
              <th scope="col">Agregar a lista</th>
            </tr>
          </thead>
          <tbody>

            <?php

              $catalogos=simplexml_load_file("CatalogoDeCanciones.xml");
              foreach ($catalogos as $catalogo) {
                echo '<tr>';
                echo '<th scope="row">'.$catalogo['id'].'</th>';
                echo '<td>'.$catalogo->cancion.'</td>';
                echo '<td>'.$catalogo->artista.'</td>';
                echo '<td>'.$catalogo->genero.'</td>';
                echo '<td><button class="btn btn-info" onclick="agregar('.$catalogo['id'].')"; >+</button></td>';
                echo '</tr>';
              }

              ?>
          </tbody>
        </table>
    </div>

    <!-- Librerias JS -->
	<script src="js/jquery-3.4.1.min.js"></script>
	<script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script>
    function agregar(id){
    $.ajax({
            type: "POST",
            dataType: 'html',
            url: "agregar.php",
            data: "id="+id,
            success: function(resp){
               alert('Listo');
            }
        });
    }

  </script>

</body>
</html>