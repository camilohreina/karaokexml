<?php

    session_start();

    //COMPRUEBA QUE EL USUARIO ESTA AUTENTIFICADO
    if ($_SESSION["auth"] != "si") {
        //si no existe, se dirige a la P�gina de Inicio
        header("Location: login.php");
        //salimos del script
        exit();
    }	
    $id=$_POST['id'];
    $eleccion=array();
    $catalogos=simplexml_load_file("CatalogoDeCanciones.xml");
    foreach ($catalogos as $catalogo) {
    	// echo $tarea['id'];
    	if ($catalogo['id']==$id) {
            $eleccion['id']= $catalogo['id'];
            $eleccion['genero']= $catalogo->genero;
            $eleccion['artista']= $catalogo->artista;
            $eleccion['cancion']= $catalogo->cancion;
            $eleccion['url']= $catalogo->url; 
    	}
    }

    if(isset($eleccion)){
        $load=simplexml_load_file("ListaDeReproduccion.xml");
        $hijo=$load->addchild("reproduccion");
        $hijo->addAttribute("id", $id);
        $hijo->addChild("genero",$eleccion['genero']);
        $hijo->addChild("artista",$eleccion['artista']);
        $hijo->addChild("cancion",$eleccion['cancion']);
        $hijo->addChild("url", $eleccion['url']);
        $hijo->addChild("mesa", $_SESSION["mesa"]);
        $load->asXML("ListaDeReproduccion.xml");
    }
 ?>